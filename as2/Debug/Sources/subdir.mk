################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Sources/ADCs.c \
../Sources/FTMs.c \
../Sources/I2Cs.c \
../Sources/IntToString.c \
../Sources/LEDs.c \
../Sources/Switches.c \
../Sources/UARTs.c \
../Sources/main.c 

OBJS += \
./Sources/ADCs.o \
./Sources/FTMs.o \
./Sources/I2Cs.o \
./Sources/IntToString.o \
./Sources/LEDs.o \
./Sources/Switches.o \
./Sources/UARTs.o \
./Sources/main.o 

C_DEPS += \
./Sources/ADCs.d \
./Sources/FTMs.d \
./Sources/I2Cs.d \
./Sources/IntToString.d \
./Sources/LEDs.d \
./Sources/Switches.d \
./Sources/UARTs.d \
./Sources/main.d 


# Each subdirectory must supply rules for building sources it contributes
Sources/%.o: ../Sources/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -D"CPU_MK64FN1M0VMD12" -I"../Sources" -I"../Project_Settings/Startup_Code" -I"../SDK/platform/CMSIS/Include" -I"../SDK/platform/devices" -I"../SDK/platform/devices/MK64F12/include" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


