/*
 * LEDs.h
 *
 *  Created on: Mar 26, 2018
 *      Author: EvanK
 */

#ifndef SOURCES_LEDS_H_
#define SOURCES_LEDS_H_

void init_all_leds();
void rled_init();
void bled_init();
void gled_init();
void rled_tog();
void bled_tog();
void gled_on();
void gled_tog();

#endif /* SOURCES_LEDS_H_ */
