/*
 * ADCs.c
 *
 *  Created on: Mar 26, 2018
 *      Author: Evan Kennedy
 *
 * Code modified from lab
 *
 * ports used
 *
 *
 */
#include "fsl_device_registers.h"
#include "ADCs.h"

/**
 * by Evan Kennedy
 *
 * Code adapted from lab
 *
 * Initializes ADC0
 * uses adc0_dp1
 */
void ADC0_Init(){
	//ADC0_SE4b (Port C2) (CPU Pin 72)
	SIM_SCGC6 |= (0x01 << 27); //Enable clock to the ADC0 module bit 27
	//SIM_SCGC6_ADC0_MASK

	//If necessary set up the pin multiplexer and enable the port

	//Set the clock, sample timing and conversion mode
	//When DIFF=0:It is single-ended 10-bit conversion   (bit3) 8
	ADC0_CFG1 &= 0x00;
	ADC0_CFG1 |= (0x1 << 5); //  clock/2
	ADC0_CFG1 |= (0x01 << 4);//set long sample time
	ADC0_CFG1 |= (0x01 << 2);//12bit , comment this line out for 8bit

	ADC0_CFG2 = 0x00;//Select normal or fast conversion

	//Select software/hardware trigger, DMA, Compare, voltage reference, continuous conversons
	ADC0_SC2 &= 0x00;//sw trigger

	//(SC3) controls the calibration, continuous convert, and hardware averaging functions of the ADC module.
	ADC0_SC3 = 0;

	//Select the channel to convert (Single ended or diff.)
	ADC0_SC1A &= 0x0;
	ADC0_SC1A |= 0x01;//ADC0_DP1 is input, pin 1 on j4 header
}



/** by Evan Kennedy
 *
 * Code adapted from lab
 * uses adc1_dp1
 */
void ADC1_Init(){
	SIM_SCGC3 |= SIM_SCGC3_ADC1_MASK; //Enable clock to module
	//If necessary set up the pin multiplexer and enable the port

	//Set the clock, sample timing and conversion mode
	//When DIFF=0:It is single-ended 10-bit conversion   (bit3) 8
	ADC1_CFG1 &= 0x00;
	ADC1_CFG1 |= (0x1 << 5); //  clock/2
	ADC1_CFG1 |= (0x01 << 4);//set long sample time
	ADC1_CFG1 |= (0x01 << 2);//12bit , comment this line out for 8bit

	ADC1_CFG2 = 0x00;//Select normal or fast conversion

	//Select software/hardware trigger, DMA, Compare, voltage reference, continuous conversons
	ADC1_SC2 &= 0x00;//sw trigger

	//(SC3) controls the calibration, continuous convert, and hardware averaging functions of the ADC module.
	ADC1_SC3 = 0;

	//Select the channel to convert (Single ended or diff.)
	ADC1_SC1A &= 0x0;
	ADC1_SC1A |= 0x01;//ADC1_DP1 is input, pin5 on j4 header
}

/** By Evan Kennedy
 *
 *Converts the analog signal into a digital value, for ADC0
 * returns integer 0-4095 if ADC is in 12bit mode
 */
unsigned int ADC0_Convert(){
	ADC0_SC1A = 0x1 & ADC_SC1_ADCH_MASK; //Write to SC1A to start conversion
	while(!(ADC0_SC1A & ADC_SC1_COCO_MASK)){}//Wait for conversion complete
	return ADC0_RA;
}

/** By Evan Kennedy
 *
 * Converts the analog signal into a digital value
 * returns integer 0-4095 if ADC is in 12bit mode
 */
unsigned int ADC1_Convert(){
	ADC1_SC1A = 0x1 & ADC_SC1_ADCH_MASK; //Write to SC1A to start conversion
	while(!(ADC1_SC1A & ADC_SC1_COCO_MASK)){}//Wait for conversion complete
	return ADC1_RA;
}

