/*
 * LEDs.c
 *
 *  Created on: Mar 26, 2018
 *      Author: EvanK
 *
 *  ports used
 *  PORTB 22
 *  PORTB 21
 *  PORTE 26
 */

#include "fsl_device_registers.h"
#include "LEDs.h"

/** * EvanK
 * initializes all leds */
void init_all_leds(){
	rled_init();
	bled_init();
	gled_init();
}

/**  EvanK
 * uses ptb22
 * initialize red led */
void rled_init(){
	SIM_SCGC5 |= SIM_SCGC5_PORTB_MASK; // 1. enabling clock to PORT B  pg314
	//ALT1 PTB22
	PORTB_PCR22 |= PORT_PCR_MUX(1) | PORT_PCR_DSE_MASK; // 2. multiplexing functionality to pin
	GPIOB_PDDR |= (0x01 << 22); // 3. enable pin 22 as output
	GPIOB_PSOR |= (0x01 << 22); //set led bit (off)
}

/** EvanK
 * uses ptb21
 * initializes blue led */
void bled_init(){
	SIM_SCGC5 |= SIM_SCGC5_PORTB_MASK; // 1. enabling clock to PORT B  pg314
	PORTB_PCR21 |= PORT_PCR_MUX(1) | PORT_PCR_DSE_MASK; // 2. multiplexing functionality to pin
	GPIOB_PDDR |= (0x01 << 21); // 3. enable pin 22 as output
	GPIOB_PSOR |= (0x01 << 21); //set led bit (off)
}

/** * EvanK
 * uses pte26
 * initializes green led*/
void gled_init(){
	SIM_SCGC5 |= SIM_SCGC5_PORTE_MASK; // 1. enabling clock to PORT B  pg314
	PORTE_PCR26 |= PORT_PCR_MUX(1) | PORT_PCR_DSE_MASK; // 2. multiplexing functionality to pin
	GPIOE_PDDR |= (0x01 << 26); // 3. enable pin 22 as output
	GPIOE_PSOR |= (0x01 << 26); //set led bit (off)
}

/** EvanK
 * toggle red led */
void rled_tog(){
	GPIOB_PTOR = 0x01 << 22; //toggle led bit
}

/** EvanK
 * toggle blue led */
void bled_tog(){
	GPIOB_PTOR = 0x01 << 21; //toggle led bit
}

/** EvanK
 * turn on green led */
void gled_on(){
	GPIOE_PCOR |= (0x01 << 26); //clear led bit
}

/** EvanK
 * toggle green led */
void gled_tog(){
	GPIOE_PTOR = 0x01 << 26; //toggle led bit
}

