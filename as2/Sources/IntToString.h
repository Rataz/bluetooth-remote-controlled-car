/*
 * IntToString.h
 *
 *  Created on: Mar 26, 2018
 *      Author: EvanKennedy
 */

#ifndef SOURCES_INTTOSTRING_H_
#define SOURCES_INTTOSTRING_H_

void Dibble_Dabble2(unsigned int a , char array[] );
void Dibble_Dabble(unsigned int a);
char getBit( int num , int n);

#endif /* SOURCES_INTTOSTRING_H_ */
