#include "fsl_device_registers.h"
#include "LEDs.h"
#include "FTMs.h"
#include "ADCs.h"
#include "Switches.h"
#include "UARTs.h"
#include "IntToString.h"
#include "I2Cs.h"

//function prototypes
void EnableAllPorts();
void PatternMove();
void interPret_buffr( char array[]  );
int interpret_segment( char array[] , int start , int end );
void SetMotorPower(unsigned int left ,unsigned int right);
void recieve_serial_input( char input );

/**
 * By Evan Kennedy
 */

//note: DAC possible values range from 0-4095 (0-FFF) (12bit)

/* Config variable, modify this depending if the board is to be a Car,or Car controller
 *
 * Modes are:
 * 0: Debug
 * 1: Remote Controller for RC Car
 * 2: RC Car
 */
int mode = 2;

int main(void){
	EnableAllPorts();
	init_all_leds();

	//initialization
	if( mode==0 ){
		//debug mode, for experimental code without modifying working vehicle/controller code
		UART0_Interface_Init();
		ADC0_Init();
		ADC1_Init();
		I2C1_init();
		I2C0_init();
	}
	else if( mode==1 ){
		//remote controller mode
		ADC0_Init();
		ADC1_Init();
		UART4_Init_Bluetooth();
		switch2_init();
		initSwitchEdgeDetect();
	}
	else if(mode==2){
		//vehicle mode
		FTM0_init_freerun();
		ADC0_Init();
		I2C1_init();
		I2C0_init();
		UART4_Init_Bluetooth();
	}

	//main loop
	int p=1;
	while (p>0) {
		if( mode==0 ){//test mode

			char left[3];
			char right[3];

			Dibble_Dabble2( ADC0_Convert() ,  left );
			Dibble_Dabble2( ADC1_Convert() , right );

			char string[] = { '<' , left[0] , left[1] , left[2] , left[3] , ',' , right[0] , right[1] , right[2], right[3] , '>' };
			UART0_Putstring( string , sizeof(string) );

			//put all the characters of the string, for a complete round trip of analog-digital-analog on 1 board
			//must have both pot
			recieve_serial_input( string[0] ) ;
			recieve_serial_input( string[1] ) ;
			recieve_serial_input( string[2] ) ;
			recieve_serial_input( string[3] ) ;
			recieve_serial_input( string[4] ) ;
			recieve_serial_input( string[5] ) ;
			recieve_serial_input( string[6] ) ;
			recieve_serial_input( string[7] ) ;
			recieve_serial_input( string[8] ) ;
			recieve_serial_input( string[9] ) ;
			recieve_serial_input( string[10] ) ;
		}
		else if( mode==1 ){//this device is a controller

			//send this data to the vehicle
			char left[3];
			char right[3];

			//TODO: add code to not send if value not changed significantly (could save bandwidth, improve responsiveness)

			Dibble_Dabble2( ADC0_Convert() ,  left );
			Dibble_Dabble2( ADC1_Convert() , right );

			//output string to vehicle
			char string[] = { '<' , left[0] , left[1] , left[2] , left[3] , ',' , right[0] , right[1] , right[2], right[3] , '>' };

			//transmit string
			UART4_Putstring( string , sizeof(string) );
		}
		else if(mode==2){//this device is a vehicle,

			//listen for transmissions, change argument to UART4 for bluetooth, UART0 for pc putty terminal for debug
			recieve_serial_input( UART4_Recieve() ) ;
			//UART0_Putstring( string , sizeof(string) );
		}
	}
	return 0;
}


//buffer to accumulate commands from user
char bytesRecieved[] = {  '1','1','1','1', '1',  '1','1','1','1' };
int index_buffr = 0;
int haveStart = 0; //a start character has been recieved

/** EvanK
 * Parse character string data received,
 * similar concept to turing machine  */
void recieve_serial_input( char input ){
	if( haveStart ){
		//start command sequence character recieved previously

		if(input >= '0' && input <= '9'  ){//data characters
			bytesRecieved[index_buffr] = input;
			index_buffr++;
		}
		else if(input == ','){ //middle separator character
			bytesRecieved[index_buffr] = input;
			index_buffr++;
		}
		else if(input == '>'){ //end character
			haveStart=0;
			index_buffr=0;
			interPret_buffr(bytesRecieved);//done
		}
	}

	//start character recieved
	if(input == '<'){
		haveStart=1;
		index_buffr=0;
	}

	//Pattern movement command recieved
	if(input == 'P'){
		haveStart=0;
		index_buffr=0;
		PatternMove();
	}

	//start from scratch if overflow of buffer
	if( index_buffr>sizeof(bytesRecieved) ){
		index_buffr=0;
		haveStart=0;
	}
}

/** Evan Kennedy
 *
 * From a character array, grab a set of number characters
 * and create an integer from them
 * ie   "125"->int 125   */
int interpret_segment( char array[] , int start , int end ){
	int multiplier = 1;
	int result = 0;

	//iterate over the substring
	for( int i=end; i>=start; i--  ){
		result+= (array[i] & 0x0F) * multiplier;
		multiplier *=10;
	}
	return result;//return integer representation
}


//config for vehicle, set to 0 if no DACs connected to board
int useI2Cs = 1;

/**
 * Evan Kennedy
 * Parses array of size 9 into Left+Right motor power levels
 * and sets those levels on the DACs
 *
 * input: array of characters representing a command recieved from UART
 */
void interPret_buffr( char array[]  ){
	//note: make first argument 0, or 5
	int left = interpret_segment( array , 0, 3 );//get data for left DAC
	int right = interpret_segment( array , 5, 8 );//get data for right DAC

	//enable if connected
	if(useI2Cs){
		int distance  = ADC0_Convert();

		//set motorpower to 0 if obstacle close
		if( distance < 2500 )
			SetMotorPower( left , right);
		else
			SetMotorPower( 0 , 0);
	}
}


/** EvanK
 * set the power levels of the motors via the mcp4725 DACs   */
void SetMotorPower(unsigned int left ,unsigned int right){
	set_I2C1_output ( left );
	set_I2C0_output ( right );
}

/** Evan Kennedy
 * moves the vehicle in a certain pattern,
 * returns when pattery complete
 */
void PatternMove(){
	//Parrallel arrays to store the sequence of motor power levels
	int pwrL[]={0,4095,   0,4095,0}; // left motor
	int pwrR[]={0,   0,4095,4095,0}; //right motor
	int idx_pwr=0;//index to increment at each interval

	while(idx_pwr<4){
		int distance  = ADC0_Convert();

		if( distance > 2500 ){
			//stop distance
			SetMotorPower( 0 , 0 );
		}
		else{
			SetMotorPower(  pwrL[idx_pwr] , pwrR[idx_pwr]);
			idx_pwr++;
			delay(1);
		}
	}
}

/** Evan Kennedy
 * Enables all the ports ABCDE of the k64   */
void EnableAllPorts(){
	SIM_SCGC5 |= SIM_SCGC5_PORTA_MASK;
	SIM_SCGC5 |= SIM_SCGC5_PORTB_MASK;
	SIM_SCGC5 |= SIM_SCGC5_PORTC_MASK;
	SIM_SCGC5 |= SIM_SCGC5_PORTD_MASK;
	SIM_SCGC5 |= SIM_SCGC5_PORTE_MASK;
}

/** EvanK
 * handles interrupts on portC
 * sends Pattern Movement signal to the vehicle*/
void PORTC_IRQHandler(){
	PORTC_PCR6 |= PORT_PCR_ISF_MASK;//clear interrupt flag by writing 1 to it
	UART4_Putchar( 'P' );
}
/** EvanK
 * toggles green led, unused */
void PORTA_IRQHandler(){
	PORTA_PCR4 |= PORT_PCR_ISF_MASK;//clear interrupt flag by writing 1 to it
	gled_tog();
}







