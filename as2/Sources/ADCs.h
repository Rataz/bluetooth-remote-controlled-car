/* ADCs.h
 *  Created on: Mar 26, 2018
 *      Author: EvanK
 */

#ifndef SOURCES_ADCS_H_
#define SOURCES_ADCS_H_

void ADC0_Init();
void ADC1_Init();
unsigned int ADC0_Convert();
unsigned int ADC1_Convert();

#endif /* SOURCES_ADCS_H_ */
