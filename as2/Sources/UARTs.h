/*
 * UARTs.h
 *
 *  Created on: Mar 26, 2018
 *      Author: EvanK
 */

#ifndef SOURCES_UARTS_H_
#define SOURCES_UARTS_H_

void UART0_Interface_Init();
void UART4_Init_Bluetooth();
char UART4_Recieve();
char UART0_Recieve();
void UART0_Putchar(char character);
void UART0_Putstring( char array[] , int size );
void UART4_Putchar(char character);
void UART4_Putstring( char array[] , int size );
void UART0_NewLine();

#endif /* SOURCES_UARTS_H_ */
