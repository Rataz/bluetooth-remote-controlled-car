/*
 * FTMs.h
 *
 *  Created on: Mar 26, 2018
 *      Author: EvanK
 */

#ifndef FTMS_H_
#define FTMS_H_

void FTM0_init_freerun();
void delay(double t);

#endif /* FTMS_H_ */
