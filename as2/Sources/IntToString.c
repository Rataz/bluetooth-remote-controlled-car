/*
 * IntToString.c
 *
 *  Created on: Mar 26, 2018
 *      Author: EvanK
 */

#include "fsl_device_registers.h"
#include "IntToString.h"
#include "UARTs.h"

/* EvanK
 *
 * Gets the character representation of a bit in an integer
 *
 * num: input number
 * n: bit of num to get back as character '0' or '1'   */
char getBit( int num , int n){
	int bitN = num & (1 << n);
	if(bitN>0){
		return '1';
	}
	else{
		return '0';
	}
}

/** Evan Kennedy
 * An implementation of 'shift and add3' algorith, or 'double-dabble'
 * result is sent to putty terminal on uart0
 *
 * based on the following:
http://www.eng.utah.edu/~nmcdonal/Tutorials/BCDTutorial/BCDConversion.html
http://www.johnloomis.org/ece314/notes/devices/binary_to_BCD/bin_to_bcd.html
http://www.classiccmp.org/cpmarchives/cpm/mirrors/cbfalconer.home.att.net/download/dubldabl.txt
 * doesnt work for 5 digit numbers currently
 */
void Dibble_Dabble(unsigned int a){
	unsigned int column[]={0,  0,0,0,0, 0,0,0,0, 0,0};
	unsigned int x = a;

	//i=bits
	for(int i=0;i<16;i++){

		//i=columns
		for(int i=1; i<11; i++){
			if(column[i]>=5)
				column[i]+=3;
		}

		//j=columns-1
		//greatest to least
		for(int j=10; j>0; j--){

			column[j] = column[j] << 1;
			column[j] &= 0x0F; //erase first bits

			if(j!=1){
				//test end of first byte
				if( column[j-1] & 0x08 ){
					column[j] |= 0x01;
				}
			}
			else{
				//test with top bit of input
				//0x80 for 8bit
				if( x & 0x8000 ){
					column[j] |= 0x01;
				}
				x = x << 1;
			}
		}
	}
	char mask = 0x30;
	UART0_Putchar(mask | column[6]);
	UART0_Putchar(mask | column[5]);
	UART0_Putchar(mask | column[4]);
	UART0_Putchar(mask | column[3]);
	UART0_Putchar(mask | column[2]);
	UART0_Putchar(mask | column[1]);
	UART0_NewLine();
}

/* Evan Kennedy
 * 4 Digits only version, used for converting integer to string for transmission to vehicle
 *
 * a: number to be converted to a 0's padded 4 character string
 * result of operation is placed in 'array'   */
void Dibble_Dabble2(unsigned int a , char array[] ){
	unsigned int column[]={0,  0,0,0,0, 0,0,0,0, 0,0};
	unsigned int x = a;

	//i=bits
	for(int i=0;i<16;i++){

		//i=columns
		for(int i=1; i<11; i++){
			if(column[i]>=5)
				column[i]+=3;
		}

		//j=columns-1
		//greatest to least
		for(int j=10; j>0; j--){

			column[j] = column[j] << 1;
			column[j] &= 0x0F; //erase first bits

			if(j!=1){
				//test end of first byte
				if( column[j-1] & 0x08 ){
					column[j] |= 0x01;
				}
			}
			else{
				//test with top bit of input
				//0x80 for 8bit
				if( x & 0x8000 ){
					column[j] |= 0x01;
				}
				x = x << 1;
			}
		}
	}
	char mask = 0x30; //mask to turn 1 digit number into an ascii character
	array[0] = (mask | column[4]);
	array[1] = (mask | column[3]);
	array[2] = (mask | column[2]);
	array[3] = (mask | column[1]);
}
