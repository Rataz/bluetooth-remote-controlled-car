/* Switches.c
 *
 *  Created on: Mar 26, 2018
 *      Author: EvanK
 *  Ports used: ptc6
 */

#include "fsl_device_registers.h"
#include "Switches.h"

/** EvanK
 * initializes switch2, uses ptc6
 */
void switch2_init(){
	SIM_SCGC5 |= SIM_SCGC5_PORTC_MASK; //enable clock
	PORTC_PCR6 |= PORT_PCR_MUX(1) | PORT_PCR_DSE_MASK; //multiplex functionality
	GPIOC_PDDR &= ~(0x01 << 06); // 3. enable pin 6 as input
}

/**
 * EvanK
 * initalize interrupts on switch press edge
 * see pg283 k64 reference manual
	0b1011 either edge
	0b1001 rising edge
	0b1010 falling edge
	0b1100 logic 1
	0b1000 logic 0
 */
void initSwitchEdgeDetect(){
	PORTC_PCR6 |= PORT_PCR_IRQC(0b1010);
	NVIC_EnableIRQ(PORTC_IRQn);
}

