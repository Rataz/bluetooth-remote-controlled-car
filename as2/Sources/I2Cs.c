/* I2Cs.c
 *  Created on: Mar 27, 2018
 *      Author: Evan Kennedy
 */

#include "I2Cs.h"
#include "fsl_device_registers.h"

/**
 * I2C notes:
 *
 * Available pinout on k64 for i2cs:
 * pte0 i2c1 sda alt6
 * pte1 i2c1 scl alt6
 * pta12 i2c2 scl alt5
 * pta13 i2c2 sda alt5
 * pta14 i2c2 scl alt5
 * ptb0 i2c0 scl alt2
 * ptb1 i2c0 sda alt2
 * ptb2 i2c0 scl alt2
 * ptb3 i2c0 sda alt2
 *
 * //in use for uart4(bluetooth)
 * pte24 i2c0 scl alt5
 * pte25 i2c0 sda alt5
 *
 * //in use for DAC2
 * ptd2 i2c0 scl alt7
 * ptd3 i2c0 sda alt7
 *
 * //in use for DAC1
 * ptc10 i2c1
 * ptc11 i2c1
 *
 */

/**
 * By Evan Kennedy
 *
 * Initialize I2C1
 *
 * USING
 * PTC10 Alt2  SCL I2C1
 * PTC11 Alt2  SDA I2C1
 *
 * External DAC info (MCP4725)---
 * 2.7V to 5.5V operating voltage
 * 12bit input  0-4095
 *
 * D Input Code (0 to 4095)
 * Vout = Vdd*(D/4095)
 *
 *see si prefixes
 *
 * 0-100 khz
 * start hold min 4000ns
 * stop hold min  4000ns
 * data hold max 3450ns
 *--------------------------------
 *--------------------------------
 *REFMANUAL: CH51 pg 1525
 *Module Initialization (Master)
1. Write: Frequency Divider register to set the I2C baud rate (see example in
description of ICR)
2. Write: Control Register 1 to enable the I2C module and interrupts
3. Initialize RAM variables (IICEN = 1 and IICIE = 1) for transmit data
4. Initialize RAM variables used to achieve the routine shown in the following figure
5. Write: Control Register 1 to enable TX
6. Write: Control Register 1 to enable MST (master mode)
7. Write: Data register with the address of the target slave (the LSB of this byte
determines whether the communication is master receive or transmit)

Code partially based on :
https://community.nxp.com/thread/390659
 *
 */
void I2C1_init()
{
	SIM_SCGC4 |= SIM_SCGC4_I2C1_MASK;
	SIM_SCGC5 |= SIM_SCGC5_PORTC_MASK;
	PORTC_PCR10 |= PORT_PCR_MUX(2);
	PORTC_PCR11 |= PORT_PCR_MUX(2);

	//1 Write: Frequency Divider register to set the I2C baud rate (see example indescription of ICR)
	I2C1_F = 0x22;

	//2 Write: Control Register 1 to enable the I2C module and interrupts
	//3 Initialize RAM variables (IICEN = 1 and IICIE = 1) for transmit data
	I2C1_C1 |= I2C_C1_IICEN_MASK; //enable i2c module operation
	I2C1_C1 |= I2C_C1_IICIE_MASK; //enable i2c module interrupts

	//4 Initialize RAM variables used to achieve the routine shown in the following figure
	I2C1_C1 |= I2C_C1_TX_MASK; // 5 transmit mode
	I2C1_C1 |= I2C_C1_MST_MASK; // 6 master mode

	NVIC_EnableIRQ(I2C1_IRQn);//assign interrupt

	//I2C1_D = 0x0;
	while( !(I2C1_S & I2C_S_TCF_MASK) ){}//wait till set

	//byte1
	//address code + address + write(0) to talk to mcp4725
	I2C1_D = 0xC0;
}


/**
 * By Evan Kennedy
 *
 * Initialize I2C0
* USING:
 * PTD2 i2c0 scl alt7
 * PTD3 i2c0 sda alt7
 */
void I2C0_init()
{
	SIM_SCGC4 |= SIM_SCGC4_I2C0_MASK; //enable clock to module
	PORTD_PCR2 |= PORT_PCR_MUX(7); //SCL
	PORTD_PCR3 |= PORT_PCR_MUX(7); //SDA

	//1 Write: Frequency Divider register to set the I2C baud rate (see example indescription of ICR)
	I2C0_F = 0x22;

	//2 Write: Control Register 1 to enable the I2C module and interrupts
	//3 Initialize RAM variables (IICEN = 1 and IICIE = 1) for transmit data
	I2C0_C1 |= I2C_C1_IICEN_MASK; //enable i2c module operation
	I2C0_C1 |= I2C_C1_IICIE_MASK; //enable i2c module interrupts

	//4 Initialize RAM variables used to achieve the routine shown in the following figure
	I2C0_C1 |= I2C_C1_TX_MASK; // 5 transmit mode
	I2C0_C1 |= I2C_C1_MST_MASK; // 6 master mode

	NVIC_EnableIRQ(I2C0_IRQn);//assign interrupt

	//I2C1_D = 0x0;
	while( !(I2C0_S & I2C_S_TCF_MASK) ){
		//wait till set
	}

	//address code + address + write(0) to talk to mcp4725  //byte1
	I2C0_D = 0xC0;

}


/** By Evan Kennedy
 *
 * From an integer, do the required shifting to create the byte to send to the dac.
 * Multiple calls will be neccesary for the same input number due to the serial nature
 * of the DAC.  Creates bytes 3 and 4 of data for the MCP4725
 *
 * num: input number
 * byte: index of the byte to be created
 *
 * ex: ( 132, 2 )  create the lsb data byte
 */
unsigned int create_dac_data_byte( int num , int byte ){
	unsigned int res = 0x00; //final result
	if(byte == 2){//msb
		res = (( num & 0x0FF0 ) >> 4);
	}
	else if(byte == 3){//lsb
		res = (( num &  0x000F ) << 4);
	}
	return res;
}

//                |  dont touch |  msb  , lsb
// msb mask is 0xFF
// lsb mask is 0xF0
// overall maximum value to send is 0xFF, then 0xF0
//
// 0xC0 is address
// 0x40 is write command
int i2c1_bytes[] = { 0xC0 , 0x40 , 0x00, 0x00 };
int i2c0_bytes[] = { 0xC0 , 0x40 , 0x00, 0x00 };
int output_i2c1 = 0x0000;// 00 to FFF  ie 0 - 4095
int output_i2c0 = 0x0000;// 00 to FFF  ie 0 - 4095
int byte_index_i2c1 = 1; //initialize to 1 because first byte is data
int byte_index_i2c0 = 1; //initialize to 1 because first byte is data

/**
 * By Evan Kennedy
 * Used for communication with main, rather than external variable
 */
void set_I2C1_output (  int data ){	output_i2c1=data; }
void set_I2C0_output (  int data ){	output_i2c0=data; }

/**
 * By Evan Kennedy
 *
 * I2C1 interrupt handler
 * Sends data to DAC byte after byte after each ACK reply
 * Also clears interrupt flags
 */
void I2C1_IRQHandler(){
	if( I2C1_S & I2C_S_IICIF_MASK ){
		I2C1_S |= I2C_S_IICIF_MASK;//clear interrupt flag by writing 1
	}

	if( I2C1_S & I2C_S_TCF_MASK ){
		//transmission is complete, send the next byte

		if( byte_index_i2c1 > 3){
			//update data bytes
			i2c1_bytes[2] = create_dac_data_byte( output_i2c1 , 2 );
			i2c1_bytes[3] = create_dac_data_byte( output_i2c1 , 3 );

			//at this point last byte has been sent, send the repeated start, to begin a new transmission
			I2C1_C1 |= I2C_C1_RSTA_MASK; //generate repeating start

			//start from the first byte (address byte) again
			byte_index_i2c1 = 0;

			//transmit byte
			I2C1_D = i2c1_bytes[byte_index_i2c1];
		}
		else{
			//transmit byte
			I2C1_D = i2c1_bytes[byte_index_i2c1];
		}
		byte_index_i2c1++;
	}

	//changing mode to recieve should generate a stop bit (untested)
	//I2C1_C1 |= I2C_C1_TX_MASK; //set transmit mode

	if( I2C1_S & I2C_S_ARBL_MASK){//arbitration lost
		//idx = 0;
		I2C1_S |= I2C_S_ARBL_MASK; //reset arbitration lost bit
		//I2C1_C1 |= I2C_C1_RSTA_MASK;
	}

	if( I2C1_S & I2C_S_BUSY_MASK ){	}//future use
}



/** By Evan Kennedy
 *
 * I2C0 interrupt handler
 * Sends data to DAC byte after byte after each ACK reply
 * Also clears interrupt flags
 *
 * USING:
 * ptd2 i2c0 SCL alt7,  pin8  on j2 header
 * ptd3 i2c0 SDA alt7,  pin10 on j2 header  */
void I2C0_IRQHandler(){
	if( I2C0_S & I2C_S_IICIF_MASK ){
		I2C0_S |= I2C_S_IICIF_MASK;//clear interrupt flag by writing 1
	}

	if( I2C0_S & I2C_S_TCF_MASK ){
		//transmission is complete, send the next byte

		if( byte_index_i2c0 > 3){
			//update data bytes
			i2c0_bytes[2] = create_dac_data_byte( output_i2c0 , 2 );
			i2c0_bytes[3] = create_dac_data_byte( output_i2c0 , 3 );

			//at this point last byte has been sent, send the repeated start, to begin a new transmission
			I2C0_C1 |= I2C_C1_RSTA_MASK; //generate repeating start

			//start from the first byte (address byte) again
			byte_index_i2c0 = 0;

			//transmit byte
			I2C0_D = i2c0_bytes[byte_index_i2c0];
		}
		else{
			//transmit byte
			I2C0_D = i2c0_bytes[byte_index_i2c0];
		}
		byte_index_i2c0++;
	}

	if( I2C0_S & I2C_S_ARBL_MASK){
		//arbitration lost
		I2C0_S |= I2C_S_ARBL_MASK; //reset arbitration lost bit
	}

	if( I2C0_S & I2C_S_BUSY_MASK ){}//for future use
}



