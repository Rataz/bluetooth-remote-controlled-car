/*
 * FTMs.c
 *
 *  Created on: Mar 26, 2018
 */

#include "fsl_device_registers.h"

#include "FTMs.h"
#include "LEDs.h"

/* FTM Notes:
 *
 * FTM pg949, chap40
 * pulse width measurement pg1065
 * pg1063 dual edge capture mode
 * BDM mode pg 1075
 *
 * extra NVIC_EnableIRQ(FTM0_IRQn);
 *
 * see also
 * https://community.nxp.com/thread/430432
 */

/** Evan Kennedy
 * FTM see pg 1005
 * initializes ftm0 as a free running counter */
void FTM0_init_freerun(){
	SIM_SCGC6 |= SIM_SCGC6_FTM0_MASK;//sim6 -> FTM0,FTM1,FTM2
	SIM_SCGC6 |= SIM_SCGC6_FTM0_MASK; //clock enable
	FTM0_MODE |= FTM_MODE_WPDIS_MASK ; //write enable
	FTM0_MODE |= FTM_MODE_FTMEN_MASK ; //ftmen=1
	FTM0_SC |= ( 0x01 << 3);  // System clock
	//NVIC_EnableIRQ(FTM0_IRQn);//enable interrupt vector
	FTM0_MOD |= 0xFFFF; //free running counter
}

/**
 * creates a 't' seconds delay with polling
 */
void delay(double t){
	double sec = t * 97650;
	FTM0_CNTIN = 0x0;
	while (sec > 0){
		if (FTM0_SC & FTM_SC_TOF_MASK){
			FTM0_SC &= !FTM_SC_TOF_MASK;
			FTM0_SC |= 0x0009;
			sec = sec - 1;
		}
	}
}
