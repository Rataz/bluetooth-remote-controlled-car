/*
 * I2Cs.h
 *
 *  Created on: Mar 27, 2018
 *      Author: Evan Kennedy
 */

#ifndef SOURCES_I2CS_H_
#define SOURCES_I2CS_H_

void I2C1_init();
void I2C0_init();
unsigned int create_dac_data_byte( int num , int byte );
void set_I2C1_output ( int data );
void set_I2C0_output ( int data );
void I2C1_IRQHandler();
void I2C0_IRQHandler();

#endif /* SOURCES_I2CS_H_ */
