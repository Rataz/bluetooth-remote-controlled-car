/*
 * UARTs.c
 *
 *  Created on: Mar 26, 2018
 *      Author: Evan Kennedy
 */

#ifndef SOURCES_UARTS_C_
#define SOURCES_UARTS_C_

#include "fsl_device_registers.h"
#include "UARTs.h"

/** Evan Kennedy
 * Code modified from lab
 * Ports used: *
 * ptb16 TX
 * ptb17 RX
 * pte25 RX
 * pte24 TX
 */
void UART0_Interface_Init(){
	//Enable the clock module
	SIM_SCGC4 |= SIM_SCGC4_UART0_MASK ;
	//or SIM_SCGC4 |= 0x1>>13;s

	//Enable port
	SIM_SCGC5 |= SIM_SCGC5_PORTB_MASK;

	//Connect the UART to the PORTB bits
	// -PCR4 -TX ptb16
	//- PCR5 �RX ptb17
	//- see pg 72 (Alt3)
	PORTB_PCR16 |= PORT_PCR_MUX(3);
	PORTB_PCR17 |= PORT_PCR_MUX(3);

	//Disable transmit enable and receive enable
	UART0_C2 &=	~(UART_C2_TE_MASK | UART_C2_RE_MASK);

	//Configure UART for 8 bits, no parity enabled
	UART0_C1 =0b00000000;

	//Set the baud rate to 9600
	//UART baud rate = UARTmoduleclock/(16x(SBR[12:0]+BFRD));
	UART0_BDH = 0;
	UART0_BDL = 0x88;

	//Enable transmit and receive	// see pg1544
	UART0_C2 |= UART_C2_TE_MASK ;
	UART0_C2 |= UART_C2_RE_MASK ;
}

/** Evan Kennedy
 * Code same as lab, but using board pins instead of usb
 */
void UART4_Init_Bluetooth(){
	/**	UART4
	RX  pte25, ptc14 alt3
	TX  pte24, ptc15 alt3
	SCGC1 bit10	 **/
	SIM_SCGC1 |= SIM_SCGC1_UART4_MASK ; //Enable the clock module
	SIM_SCGC5 |= SIM_SCGC5_PORTE_MASK; //Enable port

	//Connect the UART to the PORTE - see pg 72 (Alt3)
	PORTE_PCR24 |= PORT_PCR_MUX(3);
	PORTE_PCR25 |= PORT_PCR_MUX(3);

	//Disable transmit enable and receive enable
	UART4_C2 &=	~(UART_C2_TE_MASK | UART_C2_RE_MASK);

	//Configure UART for 8 bits, no parity enabled
	UART4_C1 =0b00000000;

	//Set the baud rate to 9600
	//UART baud rate = UARTmoduleclock/(16x(SBR[12:0]+BFRD));
	UART4_BDH = 0;
	UART4_BDL = 0x88;

	//Enable transmit and receive	// see pg1544
	UART4_C2 |= UART_C2_TE_MASK ;
	UART4_C2 |= UART_C2_RE_MASK ;
}

/* EvanK
 * code from lab, modified to use uart4, recieve 1 character from bluetooth */
char UART4_Recieve(){
	//1. Poll the RDRF (transmit data register empty) flag in UARTx_S1
	//2. Read the received character
	//- read UARTx_D
	//- this will clear RDRF
	while( !(UART4_S1 & UART_S1_RDRF_MASK) ){
		//wait so long as the register is empty
	}
	char i = UART4_D;
	return i;
}

/** EvanK
 * code from lab, recieve 1 character from putty */
char UART0_Recieve(){
	//1. Poll the RDRF (Receive Data Register Full Flag) flag in UARTx_S1
	//2. Read the received character
	//- read UARTx_D
	//- this will clear RDRF
	while( !(UART0_S1 & UART_S1_RDRF_MASK) ){
		//wait so long as the register is empty
	}
	char i = UART0_D;
	return i;
}

/** EvanK
 * code from lab
 */
void UART0_Putchar(char character){
	//1. Poll the TDRE (transmit data register empty) flag in UARTx_S1
	//2. Send the next character
	//   - write to UARTx_D
	//   - this will reset TDRE
	while(  !(UART0_S1 & UART_S1_TDRE_MASK) ){
		//wait until tdre is set
	}
	UART0_D = character;
}

/** EvanK
 * code from lab, improved to send any size string
 *
 * first parameter is string to send
 * second is total size of array
 *
 * you can use sizeof(array) to get the array size before sending
 */
void UART0_Putstring( char array[] , int size ){
	for(int x=0; x<size; x++){
		UART0_Putchar(array[x]);
	}
	UART0_NewLine();
}

/** EvanK
 * code from lab, improved to send any size string
 *
 * first parameter is string to send
 * second is total size of array
 *
 * you can use sizeof(array) to get the array size before sending (if its a char array)
 */
void UART4_Putchar(char character){
	//1. Poll the TDRE (transmit data register empty) flag in UARTx_S1
	//2. Send the next character
	//   - write to UARTx_D
	//   - this will reset TDRE
	while(  !(UART4_S1 & UART_S1_TDRE_MASK) ){
		//wait until tdre is set
	}
	UART4_D = character;
}

/*
 * EvanK
 * code from lab, improved to send any size string
 *
 * first parameter is string to send
 * second is total size of array
 *
 * can use sizeof(array) to get the array size before sending
 */
void UART4_Putstring( char array[] , int size ){
	//int d = sizeof(array)/sizeof(array[0]);
	//int d = sizeof(array);
	for(int x=0; x<size; x++){
		UART4_Putchar(array[x]);
	}
}

/** EvanK
 * starts new line in the terminal */
void UART0_NewLine(){
	UART0_Putchar('\r');
	UART0_Putchar('\n');
}

#endif /* SOURCES_UARTS_C_ */
